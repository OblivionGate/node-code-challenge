# Questions

Qs1: Explain the output of the following code and why

```js
setTimeout(function() {
  console.log("1");
}, 100);
console.log("2");
```

A: `2` followed by `1`.
`setTimeout` causes the function inside of it to trigger after 100ms, but JavaScript does not pause. The next line of code triggers a console log of `2`, and then the 100ms passes and `1` is logged.

---

Qs2: Explain the output of the following code and why

```js
function foo(d) {
  if (d < 10) {
    foo(d + 1);
  }
  console.log(d);
}
foo(0);
```

A: `10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0`.
In JavaScript, functions and variables are hoisted and thus moved to the top of the running stack. An easy way to look at this is as follows:
`foo(0)` checks to see if `0 < 10` (it is), and thus runs the `foo(0+1)` function. The function is hoisted to the top of the stack, and thus `conole.log(0)` is not triggered.
`foo(1)` checks to see if `1 < 10` (it is), and thus runs the `foo(1+1)` function. The function is hoisted to the top of the stack, and thus `conole.log(1)` is not triggered.
This continues until `foo(9+1)` is run. `foo(10)` checks to see if `10 < 10` (it isn't), and thus the `if` statement is not triggered and the `console.log(10)` occurs. The stack is then resolved in reverse order and the `console.log(9)` triggers, and then `console.log(8)`, all the way down to `console.log(0)`.

---

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
```

A: Running `foo(0)` would result in `5` in the above code. Likewise, running `foo(false)` and `foo('')` would result in `5`. Running anything that is `falsy` would trigger `5` in the above code.
If we want `foo` to default to `5` if nothing is provided, a better way of writing it would be:

```js
function foo(d = 5) {
  console.log(d);
}
```

The above function, when triggered as `foo(0)`, returns `0`. `foo(false)` returns `false`. `foo('')` returns an empty string. If nothing is provided, `foo()`, then it would return `5`.

---

Qs4: Explain the output of the following code and why

```js
function foo(a) {
  return function(b) {
    return a + b;
  };
}
var bar = foo(1);
console.log(bar(2));
```

A: `3` is returned.
The variable `bar` is a reference to `foo(1)`. `foo(1)` returns:

```js
function(b) {
  return 1 + b;
};
```

`bar(2)` thus runs the function with `b` as `2`, returning:

```js
function(2) {
  return 1 + 2;
};
```

If you run `bar(3)`, it returns `1 + 3`, which is `4`.

---

Qs5: Explain how the following function would be used

```js
function double(a, done) {
  setTimeout(function() {
    done(a * 2);
  }, 100);
}
```

A: The function `double(a, done)` is similar to a Promise. An example use case would be to handle the number that is doubled in another function as `a` is doubled and then fed into the function passed through.
Example:

```js
var printLine = n => console.log(n);

double(10, printLine);
```

If you wanted to print `n` after it was doubled, the above code would trigger that. `double(10, printLine)` runs `printLine(10 * 2)` after 100ms have passed, which returns `console.log(20)`.
The above code can also be written as:

```js
function printLine(n) {
  return console.log(n);
}

double(10, printLine);
```
