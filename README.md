# Node Code Challenge

Please do not set any environment variables before the start of using this app -- they will not be needed.

## Steps

- Clone this repository to your machine
- Run `npm i` from the command line while in the folder
- Run `npm start` to initiate both server and client
- Wait until you receive either of the following messages:
  - `🐍 Connected to database!`
  - `🐍 Database fully populated!`
- On initial load, a folder `db` will be created. This is just to house the database on a file and not contain it to memory

## Notes

### URLs

Once the app has been started, the front-end can be access via: http://localhost:3000/
API can be accessed via: http://localhost:8080/
The front-end uses a proxy for the back-end, enabling React to call it with simple `fetch` commands
A healthcheck can be accessed at http://localhost:8080/__health

### App Creation

For time-saving purposes, this app has been created with the `create-react-app` function.

### Ports

Client uses 3000, API uses 8080. If you receive a `EADDRINUSE` error, kill the API hosted on 8080

### Database Integration

I found [this resource](https://expressjs.com/en/guide/database-integration.html#mysql) very useful to work into sqlite3-express integration, as well as [this resource](https://www.sqlitetutorial.net/sqlite-nodejs/insert/) for table creation.
There is some fiddly code going on in table creation due to errors that `GB.tsv` had (including one missing tab).

### Search Times

I set the search field to use a timeout, ensuring that the API is only searched when the user has finished typing. This would normally be an env variable, but for speed of building this app I settled for `600ms`. Searches can also be triggered prematurely by hitting the `Enter` key.

### Relevance

"Sort the results by the closest name match":
I used the example `hastin` to map some logic. When searching for `hastin`, I was originally getting issues with capital letters and Hastingleigh/Hastingwood occurring before Hastings. I resolved this by ordering by `LENGTH` then `LOWER`.

### Links

- [Questions and their answers](./QUESTIONS.md)
- [Original Tasks (formally known as README.md)](./TASKS.md)
