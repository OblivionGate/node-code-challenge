import React from "react";
import { shallow } from "enzyme";
import App from "./App";
import Header from "./client/components/Header";
import Home from "./client/handlers/Home";

let wrapper;

describe("<App />", () => {
  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  it("renders a Header component", () => {
    expect(wrapper.find(Header)).toHaveLength(1);
  });

  it("renders a Home component", () => {
    expect(wrapper.find("Route").props().component).toEqual(Home);
  });
});
