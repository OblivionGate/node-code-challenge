import React, { Component } from "react";
import SearchBar from "../components/SearchBar";
import SearchResults from "../components/SearchResults";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      locations: [],
      searchTerm: ""
    };
    this.timer = null;
  }

  doSearch = () => {
    fetch(`/locations?q=${this.state.searchTerm}`)
      .then(response => {
        return response.status === 200
          ? response.json()
          : { error: `${response.status}: ${response.statusText}` };
      })
      .then(query => {
        if (query.error) {
          this.setState({ error: query.error, locations: [] });
        } else {
          this.setState({ error: null, locations: query.locations });
        }
      });
  };

  handleChange = e => {
    clearTimeout(this.timer);
    this.setState({ searchTerm: e.target.value });
    this.timer = setTimeout(() => {
      this.doSearch();
    }, 600);
  };

  handleKeyPress = e => {
    if (e.key === "Enter") {
      clearTimeout(this.timer);
      this.doSearch();
    }
  };

  render() {
    return (
      <div className="Home">
        <SearchBar
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
        />
        <SearchResults
          error={this.state.error}
          results={this.state.locations}
        />
      </div>
    );
  }
}
