import React from "react";
import { shallow } from "enzyme";
import Home from "./Home";
import SearchBar from "../components/SearchBar";
import SearchResults from "../components/SearchResults";

let wrapper;

describe("<Home />", () => {
  beforeEach(() => {
    wrapper = shallow(<Home />);
  });

  it("renders a SearchBar and SearchResults component", () => {
    expect(wrapper.find(SearchBar)).toHaveLength(1);
    expect(wrapper.find(SearchResults)).toHaveLength(1);
  });
});
