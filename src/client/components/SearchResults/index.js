import React, { Component } from "react";
import Chip from "../Chip";

export default class SearchResults extends Component {
  render() {
    const { error, results } = this.props;

    return (
      <div className="results">
        {results && results.length ? (
          <>
            <h3>Results</h3>
            {results.map((item, index) => (
              <Chip
                name={item.name}
                lat={item.lat}
                long={item.long}
                key={index}
              />
            ))}
          </>
        ) : (
          <div className="results__error">{error}</div>
        )}
      </div>
    );
  }
}
