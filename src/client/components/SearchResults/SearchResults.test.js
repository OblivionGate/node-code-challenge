import React from "react";
import { shallow } from "enzyme";
import SearchResults from "./index.js";
import Chip from "../Chip";

const error = "GENERIC ERROR";
const results = { name: "Close to Center", lat: "10", long: "-3" };

let wrapper;

describe("<SearchResults />", () => {
  it("renders an error with no results", () => {
    wrapper = shallow(<SearchResults error={error} results={[]} />);
    expect(wrapper.find(".results__error")).toHaveLength(1);
  });

  it("renders results with no error", () => {
    wrapper = shallow(<SearchResults error={null} results={[results]} />);
    expect(wrapper.find(".results__error")).toHaveLength(0);
    expect(wrapper.find(Chip)).toHaveLength(1);
    expect(wrapper.find(Chip).props()).toEqual(results);
  });
});
