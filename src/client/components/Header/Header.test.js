import React from "react";
import { shallow } from "enzyme";
import Header from "./index.js";

let wrapper;

describe("<Header />", () => {
  beforeEach(() => {
    wrapper = shallow(<Header />);
  });

  it("renders a Header div", () => {
    expect(wrapper.find(".Header")).toHaveLength(1);
  });
});
