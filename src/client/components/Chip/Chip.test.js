import React from "react";
import { shallow } from "enzyme";
import Chip from "./index.js";

const name = "Tooting Broadway Underground Station";
const lat = "51.42768";
const long = "-0.16802";

let wrapper;

describe("<Chip />", () => {
  beforeEach(() => {
    wrapper = shallow(<Chip name={name} lat={lat} long={long} />);
  });

  it("renders a chip name", () => {
    expect(wrapper.find(".chip__name").text()).toEqual(name);
  });

  it("renders a chip latitude and longitude", () => {
    expect(wrapper.find(".chip__lat").text()).toEqual(`Latitude: ${lat}`);
    expect(wrapper.find(".chip__long").text()).toEqual(`Longitude: ${long}`);
  });
});
