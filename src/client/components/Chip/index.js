import React, { Component } from "react";

export default class Chip extends Component {
  render() {
    return (
      <div className="chip">
        <div className="chip__name">{this.props.name}</div>
        <div className="chip__lat">Latitude: {this.props.lat}</div>
        <div className="chip__long">Longitude: {this.props.long}</div>
      </div>
    );
  }
}
