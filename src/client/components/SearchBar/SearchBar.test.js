import React from "react";
import { shallow } from "enzyme";
import SearchBar from "./index.js";

const change = jest.fn();
const keyPress = jest.fn();

let wrapper;

describe("<SearchBar />", () => {
  beforeEach(() => {
    wrapper = shallow(<SearchBar onChange={change} onKeyPress={keyPress} />);
  });

  it("tracks change", () => {
    wrapper.find("input").simulate("change", { target: { value: "red" } });
    expect(change).toHaveBeenCalledTimes(1);
  });

  it("tracks keypress", () => {
    wrapper.find("input").simulate("keyPress");
    expect(keyPress).toHaveBeenCalledTimes(1);
  });
});
