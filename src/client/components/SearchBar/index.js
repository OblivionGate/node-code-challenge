import React, { Component } from "react";

export default class SearchBar extends Component {
  render() {
    const { onChange, onKeyPress } = this.props;

    return (
      <div className="search">
        <input
          className="search__input"
          type="search"
          onChange={onChange}
          onKeyPress={onKeyPress}
          placeholder="input search term"
        />
      </div>
    );
  }
}
