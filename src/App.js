import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Header from "./client/components/Header";
import Home from "./client/handlers/Home";
import "./styles.scss";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" component={Home} />
        </Switch>
      </div>
    );
  }
}

export default App;
