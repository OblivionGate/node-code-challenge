const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const sqlite3 = require("sqlite3");
const fs = require("fs");
const locations = fs.readFileSync("./data/GB.tsv", "utf8").split(`\n`);

// DATABASE CREATION STARTS

let db;
const dbSchema = `CREATE TABLE IF NOT EXISTS Locations (
  name text,
  lat float,
  long float
);`;

const connectToDb = creationRequired => {
  db = new sqlite3.Database("./db/locations.db", err => {
    if (err) {
      return console.error(err.message);
    }
    console.log(
      creationRequired
        ? "🐍 Database created. Now populating..."
        : "🐍 Connected to database!"
    );
  });
};

if (fs.existsSync("./db")) {
  connectToDb(false);
} else {
  fs.mkdirSync("./db");
  connectToDb(true);

  const createItem = (name, lat, long) => {
    return `INSERT into Locations (name,lat,long) values ("${name}",${lat},${long})`;
  };

  db.run(dbSchema, () => {
    // shift to remove the header of the .tsv file
    locations.shift();
    locations.map((location, index) => {
      let name;
      let lat;
      let long;
      location.split(`\t`).map((item, index) => {
        switch (index) {
          case 2:
            // force items with "" to turn into '', which sqlite can handle
            name = item.replace(/"/g, `'`);
            break;
          case 4:
            lat = item;
            break;
          case 5:
            long = item;
            break;
          default:
            break;
        }
      });
      return db.run(createItem(name, lat, long), function(err) {
        if (err) {
          // debugging below, to locate possible issues
          console.error("Index:", index, name, lat, long);
          console.error("Error at index", index, err);
        }
      });
    });
    console.log("🐍 Database fully populated!");
  });
}

// DATABASE CREATION ENDS

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "build")));

app.get("/__health", function(req, res) {
  return res.send(JSON.stringify({ message: "API is alive" }));
});

app.get("/locations", async (req, res) => {
  let query = req.query.q;
  let limit = req.query.limit || 10;

  res.setHeader("Content-Type", "application/json");

  if (query && query.length > 1) {
    const sqlQuery = `
      SELECT name, lat, long
      FROM Locations
      WHERE name
      LIKE '${query}%'
      ORDER BY LENGTH (name), LOWER (name)
      LIMIT ${limit};
    `;

    db.all(sqlQuery, [], (error, locations) => {
      if (error) {
        console.error("🤖 FAIL: Error is:", error);
        res.send(JSON.stringify({ error }));
      } else {
        if (locations.length) {
          console.log(`🤖 SUCCESS: Query ${query} returned results`);
          res.send(JSON.stringify({ locations }));
        } else {
          console.error(`🤖 FAIL: Query ${query} returned no results`);
          res.send(JSON.stringify({ error: "No results found" }));
        }
      }
    });
  } else {
    console.error(`🤖 FAIL: Query requires at least 2 characters`);
    res.send(JSON.stringify({ error: "Please input at least 2 characters" }));
  }
});

app.get("/", function(req, res) {
  console.error(`🤖 FAIL: No route requested`);
  return res.send(JSON.stringify({ error: "No route requested" }));
});

app.listen(process.env.PORT || 8080, () => {
  console.log(`🤖 Server awake on port ${process.env.PORT || 8080}`);
});
